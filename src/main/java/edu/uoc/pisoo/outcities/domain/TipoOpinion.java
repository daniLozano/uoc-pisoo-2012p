package edu.uoc.pisoo.outcities.domain;

public enum TipoOpinion {
    meGusta, noMeGusta
}
