package edu.uoc.pisoo.outcities.domain.test;

import edu.uoc.pisoo.outcities.domain.*;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

public class UsuarioTest {

    private Usuario maria;
    private Usuario hugo;
    private Plan p15;
    private Plan p16;

    @Before
    public void init(){
        this.maria = new Usuario(12L, "Maria", "1234", new DateTime(1983, 12, 4, 12, 43), new EmailAddress("maria@example.com"));
        this.hugo = new Usuario(14L, "Hugo", "1234", new DateTime(1983, 12, 4, 12, 43), new EmailAddress("hugo@example.com"));
        this.p15 = new Plan(15L);
        this.p16 = new Plan(16L);
    }

    @Test
    public void testDatosUsuario(){
        Long id = 12L;
        String nombre = "Maria Garcia";
        String contrasena = "1234";
        DateTime fechaNacimiento = new DateTime(1983, 12, 4, 12, 43);
        EmailAddress email = new EmailAddress("maria@example.com");
        Usuario u = new Usuario(id, nombre, contrasena, fechaNacimiento, email);
        assertEquals(id,u.getId());
        assertEquals(nombre,u.getNombre());
        assertEquals(true, u.checkContrasena(contrasena));
        assertEquals(fechaNacimiento,u.getFechaNacimiento());
        assertTrue(u.getEdad()>20);
        assertEquals(email,u.getEmail());
        assertEquals(0,u.getFavoritos().size());
        assertEquals(0,u.getOpiniones().size());
        assertEquals(0,u.getZonas().size());
    }

    @Test
    public void testCambiarDatos(){
        String nombre = "Maria Pratt";
        DateTime fechaNacimiento = new DateTime(1983, 12, 4, 12, 40);
        EmailAddress email = new EmailAddress("maria.pratt@example.com");
        maria.cambiarDatos(nombre, fechaNacimiento, email);
        assertEquals(nombre,maria.getNombre());
        assertEquals(fechaNacimiento, maria.getFechaNacimiento());
        assertEquals(email,maria.getEmail());
    }

    @Test
    public void testCambiarContrasena(){
        String nuevaContrasena = "nueva_contrasena";
        maria.cambiarContrasena(nuevaContrasena,nuevaContrasena);
        assertEquals(true,maria.checkContrasena(nuevaContrasena));
    }

    @Test
    public void testErrorCambiarContrasena(){
        try{
            maria.cambiarContrasena("a","b");
            fail("Deberia haber lanzado excepcion!");
        }catch(IllegalArgumentException e){
            // Ok, se esperaba esta excepcion
        }
    }

    @Test
    public void testNuevaOpinion(){
        maria.opinar(p15, TipoOpinion.meGusta, "si");

        assertEquals(1, maria.getOpiniones().size());
        Opinion opinion = maria.getOpiniones().iterator().next();
        assertEquals(TipoOpinion.meGusta,opinion.getTipo());
        assertEquals("si",opinion.getComentario());

        assertEquals(1, p15.getOpiniones().size());
        Opinion opinionPlan = p15.getOpiniones().iterator().next();
        assertEquals(opinion,opinionPlan);
    }

    @Test
    public void testModificarOpinion(){
        maria.opinar(p15, TipoOpinion.meGusta, "si");
        maria.opinar(p15, TipoOpinion.meGusta, "ok");
        maria.opinar(p15, TipoOpinion.noMeGusta, "no");
        assert(maria.getOpiniones().size()==1);
        Opinion opinion = maria.getOpiniones().iterator().next();
        assertEquals(TipoOpinion.noMeGusta, opinion.getTipo());
        assertEquals("no",opinion.getComentario());
    }

    @Test
    public void testBorrarOpinion(){
        maria.opinar(p15, TipoOpinion.meGusta, "si");
        maria.opinar(p16, TipoOpinion.noMeGusta, "no");
        maria.borrarOpinion(p16);
        assertEquals(1,maria.getOpiniones().size());
        assertEquals(p15,maria.getOpiniones().iterator().next().getPlan());
    }

    @Test
    public void testAnadirFavorito(){
        maria.anadirFavorito(p15);

        assertEquals(1, maria.getFavoritos().size());
        assertTrue(maria.getFavoritos().contains(p15));

        assertEquals(1, p15.getFans().size());
        assertTrue(p15.getFans().contains(maria));
    }

    @Test
    public void testVolverAAnadirFavorito(){
        maria.anadirFavorito(p15);
        maria.anadirFavorito(p15);
        maria.anadirFavorito(p15);

        assertEquals(1, maria.getFavoritos().size());
        assertTrue(maria.getFavoritos().contains(p15));

        assertEquals(1, p15.getFans().size());
        assertTrue(p15.getFans().contains(maria));
    }

    @Test
    public void testQuitarFavorito(){
        maria.anadirFavorito(p15);
        maria.anadirFavorito(p16);
        hugo.anadirFavorito(p15);
        maria.quitarFavorito(p15);

        assertEquals(1, maria.getFavoritos().size());
        assertEquals(p16,maria.getFavoritos().iterator().next());
        assertEquals(1, p15.getFans().size());
        assertEquals(hugo,p15.getFans().iterator().next());
    }

}
